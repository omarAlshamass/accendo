<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface extends EloquentRepositoryInterface
{
    public function getStudentsIds(): Collection;
    public function getStudentHomework(User $user): Collection;
    public function submitStudentHomework(User $user, int $homeworkId): bool;
}
