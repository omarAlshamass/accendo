import React, { useState } from "react";
import Toast from '../common/components/Toast';

const withToast = WrapComponent => {
    const WithToast = props => {
        const [toastObj, setToastObj] = useState({ title: null, message: null, toast: false });

        return (<React.Fragment>
            <WrapComponent {...props} setToastObj={setToastObj} />
            <Toast toastObj={toastObj} setToastObj={setToastObj} />
        </React.Fragment>
        )
    }

    return WithToast;
}

export default withToast;