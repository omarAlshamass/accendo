<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserLoginRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Traits\ResponseTrait;

class AuthController extends Controller
{
    use ResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(UserRepositoryInterface $userRepository, UserCreateRequest $request)
    {
        return $this->response($userRepository->create(
            array_merge(
                $request->validated(),
                ['password' => bcrypt($request->password)]
            )
        ));
    }

    public function login(UserLoginRequest $request)
    {;
        if (!$token = auth()->attempt($request->validated())) {
            return $this->response(null, 'Your credential is wrong, please try again.', 401);
        }

        return $this->response([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
