<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeworkController;
use App\Http\Controllers\SubjectController;
use App\Http\Middleware\CheckIfStudent;
use App\Http\Middleware\CheckIfTeacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::prefix('homework')->middleware(CheckIfTeacher::class)->group(function () {
    Route::get('/', [HomeworkController::class, 'get']);
    Route::post('/', [HomeworkController::class, 'post']);
    Route::put('/{id}', [HomeworkController::class, 'update']);
    Route::delete('/{id}', [HomeworkController::class, 'delete']);
});

Route::prefix('/subject')->middleware(CheckIfTeacher::class)->group(function () {
    Route::get('/', [SubjectController::class, 'get']);
});

Route::prefix('studentHomework')->middleware(CheckIfStudent::class)->group(function () {
    Route::get('/', [HomeworkController::class, 'getStudentHomework']);
    Route::put('/submit/{id}', [HomeworkController::class, 'submitStudentHomework']);
});
