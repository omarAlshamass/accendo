import { backendDomain } from "../settings/config";

const subjectsRoute = backendDomain + '/subject';

export {
    subjectsRoute
};