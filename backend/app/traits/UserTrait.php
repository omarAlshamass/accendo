<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait UserTrait
{
    private $user;

    public function getUser()
    {
        if ($this->user = Auth::user())
            return $this->user;
    }
}
