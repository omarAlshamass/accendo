import { useEffect, useState } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { compose } from 'redux';
import WithPageContainer from '../../hocs/withPageContainer';
import { useHistory } from "react-router-dom";
import { waitForShowingToast, setLocalStorage, getUserRule, getFirstErrorMessage } from '../../common/utils/helper';
import { loginRoute } from '../../apis/auth';

const Login = ({ setToastObj, apiCaller }) => {
    const history = useHistory();
    const [validated, setValidated] = useState(false);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    useEffect(() => {
        localStorage.clear();
    }, []);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setValidated(true);

        if (form.checkValidity()) {
            apiCaller(loginRoute, { email, password })
                .then(({ errors, status, data }) => {
                    if (!status) {
                        setToastObj({ title: 'Error', message: getFirstErrorMessage(errors), toast: true });
                    }
                    else {
                        const { access_token, user } = data;
                        setLocalStorage('access_token', access_token);
                        setLocalStorage("user_info", JSON.stringify(user));
                        setToastObj({ title: 'Success', message: "Thanks for login, we are going to load your pages in second", toast: true });
                        waitForShowingToast(
                            () => {
                                return history.push(getUserRule(user.rule_id) === 'teacher' ? '/teacher/homework' : '/student/homework');
                            }
                        );
                    }

                })
                .catch(error => {
                    setToastObj({ title: 'Error', message: error.message, toast: true });
                }
                );
        }
    };

    return <Container>
        <Row className="justify-content-md-center text-center">
            <Col>
                <h2>Login Page</h2>
            </Col>
        </Row>
        <Row className="justify-content-md-center">
            <Col lg="6">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control required type="email" placeholder="Enter email" onChange={event => setEmail(event.target.value)} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                         </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control required type="password" placeholder="Password" onChange={event => setPassword(event.target.value)} />
                    </Form.Group>
                    <Row>
                        <Col lg={4}>
                            <Button variant="primary" type="submit">
                                Submit
                    </Button>
                        </Col>
                        <Col className="text-right" lg={8}>
                            <span>Don't have account?</span><Button variant="link" onClick={() => history.push('/register')}>Register</Button>
                        </Col>
                    </Row>
                </Form>
            </Col>
        </Row>
    </Container>
};

const enhance = compose(
    WithPageContainer
);

export default enhance(Login);