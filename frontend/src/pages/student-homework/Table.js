import { Button, Table } from 'react-bootstrap';
import { Telegram } from 'react-bootstrap-icons';
import _ from 'lodash';
import { apiCaller } from '../../common/utils/apiCaller';
import { studentHomeworkRoute } from '../../apis/homework';

const TableComponent = ({ list, setItem }) => {

    const handleSubmitAssignment = (itemObj) => {
        apiCaller(studentHomeworkRoute + '/submit/' + itemObj.id, {}, "PUT")
            .then(({ status }) => {
                if (status) {
                    setItem(itemObj);
                }
            });
    };

    return (
        <Table striped bordered hover className="text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Subject</th>
                    <th>Title</th>
                    <th>Due Date</th>
                    <th>Settings</th>
                </tr>
            </thead>
            <tbody>
                {list.map((item, itemIndex) => (
                    <tr key={itemIndex}>
                        <td>{_.get(item, 'id')}</td>
                        <td>{_.get(item, 'subject.name')}</td>
                        <td>{_.get(item, 'title')}</td>
                        <td>{_.get(item, 'due_date')}</td>
                        <td>
                            {_.get(item, 'pivot.submitted') === 0 && <Button color="blue" onClick={() => handleSubmitAssignment(item)}>
                                <Telegram /> Submit
                            </Button>}
                            {_.get(item, 'pivot.submitted') === 1 && <div>Submitted</div>}
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
}

export default TableComponent;