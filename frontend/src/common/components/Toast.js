import React from 'react';
import { Toast } from 'react-bootstrap';

const ToastComponent = ({ toastObj: { title, message, toast }, setToastObj }) => {
    return (
        <Toast
            onClose={() => setToastObj({ title: null, message: null, toast: false })}
            className="toast-message"
            show={toast}
            deplay={3000}
            autohide>
            <Toast.Header>
                <img
                    src="holder.js/20x20?text=%20"
                    className="rounded mr-2"
                    alt=""
                />
                <strong className="mr-auto">{title}</strong>
                <small>{new Date().toLocaleTimeString()}</small>
            </Toast.Header>
            <Toast.Body>{message}</Toast.Body>
        </Toast>
    )
}

export default ToastComponent;