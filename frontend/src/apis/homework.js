import { backendDomain } from "../settings/config";

const homeWorkRoute = backendDomain + '/homework';
const studentHomeworkRoute = backendDomain + '/studentHomework';

export {
    homeWorkRoute,
    studentHomeworkRoute
}