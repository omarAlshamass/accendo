import { Button, Modal } from 'react-bootstrap';

const ModalComponent = ({ show, setShow, buttonTitle, ...props }) => {

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button className="text-right" variant="primary" onClick={handleShow}>
                {buttonTitle}
            </Button>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}>
                {props.children}
            </Modal>
        </>
    );
}

export default ModalComponent;
