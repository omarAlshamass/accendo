#!/bin/bash
tput setaf 1; echo "1- The command of initialization of the containers is going to perform now."
docker-compose build && docker-compose up -d

tput setaf 1; echo "2- We are going to wait for 30 seconds to give the MySQL container some time to startup, in case the upcoming commands failed please, run them manually";
sleep 30
docker-compose exec backend php artisan migrate
docker-compose exec backend php artisan db:seed
