import React from 'react';
import Nav from './Nav';

const Main = ({ moduleTitle, moduleLink, ...props }) => {
    return (
        <React.Fragment>
            <Nav moduleTitle={moduleTitle}/>
            {props.children}
        </React.Fragment>
    );
}

export default Main;