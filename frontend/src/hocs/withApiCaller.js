import { apiCaller as apiCallerInstance } from '../common/utils/apiCaller';

const withApiCaller = WrappedComponent => {
    const WithApiCaller = props => {
        return <WrappedComponent apiCaller={apiCallerInstance} {...props} />
    }

    return WithApiCaller;
}

export default withApiCaller;