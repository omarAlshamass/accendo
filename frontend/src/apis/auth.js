import { backendDomain } from "../settings/config";

const loginRoute = backendDomain + '/auth/login';
const registerRoute = backendDomain + '/auth/register';

export {
    loginRoute,
    registerRoute
};