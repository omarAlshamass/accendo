import { Navbar, Nav } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const NavComponent = ({ moduleTitle }) => {
    const history = useHistory();

    const logoutHandler = () => {
        localStorage.clear();
        history.push('/login');
    };

    return <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
        <Navbar.Brand>{moduleTitle}</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
            <Nav>
                <Nav.Link eventKey={2} onSelect={logoutHandler}>
                    LogOut
        </Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
}

export default NavComponent;