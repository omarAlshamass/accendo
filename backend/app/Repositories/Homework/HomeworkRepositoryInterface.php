<?php

namespace App\Repositories\Homework;

use App\Repositories\EloquentRepositoryInterface;

interface HomeworkRepositoryInterface extends EloquentRepositoryInterface
{
    public function getAllWithUsersAndSubjects(int $userId);
    public function assignHomeworkToSudents(int $homeworkId, $users);
}
