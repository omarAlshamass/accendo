import React from "react";
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import TeacherHomework from "./pages/teacher-homework/TeacherHomework";
import StudentHomework from "./pages/student-homework/StudentHomework";
import { checkLoggedInUserRule, STUDENT, TEACHER } from "./common/utils/helper";

function App() {
  return (
    <Router>
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/teacher/homework" render={() => {
        return checkLoggedInUserRule() === TEACHER ? <TeacherHomework /> : <Redirect push to="/login" />
      }} />
      <Route path="/student/homework" render={() => {
        return checkLoggedInUserRule() === STUDENT ? <StudentHomework /> : <Redirect push to="/login" />
      }} />
      <Route
        exact
        path="/"
        render={() => {
          return checkLoggedInUserRule() === STUDENT
            ? <Redirect to="/student/homework" />
            : checkLoggedInUserRule() === TEACHER ?
              <Redirect to="/teacher/homework" />
              : <Redirect to="/login" />
        }} />
      <Route render={() => <Redirect to="/login" />} />
    </Router>
  );
}

export default App;
