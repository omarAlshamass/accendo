import _ from 'lodash';

const apiCaller = (url, bodyData, method = 'POST', headerParams = null) => {
    return fetch(
        url,
        {
            method: method,
            headers: headerParams ?? new Headers({
                "Accept": "application/json",
                "content-type": "application/json",
                'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
            }),
            body: _.isEmpty(bodyData) ? null : JSON.stringify(bodyData)
        }
    )
        .then(res => res.json())
        .catch(error => {
            console.log(error);
        });
}

export {
    apiCaller
}