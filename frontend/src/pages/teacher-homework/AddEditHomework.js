import _ from "lodash";
import { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { compose } from "redux";
import { homeWorkRoute } from "../../apis/homework";
import { subjectsRoute } from "../../apis/subject";
import { getFirstErrorMessage } from "../../common/utils/helper";
import WithPageContainer from "../../hocs/withPageContainer";

const AddEditHomework = ({ handleClose, apiCaller, setItem, itemToUpdate, setItemToUpdate, setToastObj }) => {
    const [validated, setValidated] = useState(false);
    const [title, setTitle] = useState(null);
    const [dueDate, setDueDate] = useState(null);
    const [selectedSubject, setSelectedSubject] = useState(null);
    const [subjects, setSubjects] = useState([]);

    useEffect(() => {
        apiCaller(subjectsRoute, {}, 'GET')
            .then(response => {
                setSubjects(response.data);
                setSelectedSubject(response.data[0].id);
            });
    }, []);

    useEffect(() => {
        setTitle(itemToUpdate.title);
        setDueDate(itemToUpdate.due_date);
        setSelectedSubject(itemToUpdate.subject_id);
    }, [itemToUpdate]);

    const handleSubmit = (event) => {
        event.preventDefault();
        setValidated(true);
        const newObj = { title, due_date: dueDate, subject_id: selectedSubject };

        if (_.isEmpty(itemToUpdate))
            apiCaller(homeWorkRoute, newObj, 'POST')
                .then(({ status, errors }) => {
                    if (status) {
                        handleClose();
                        setItem(newObj);
                    }
                    else
                        setToastObj({ title: 'Error', message: getFirstErrorMessage(errors), show: true });
                });
        else
            apiCaller(homeWorkRoute + '/' + itemToUpdate.id, newObj, 'PUT')
                .then(({ status, errors }) => {
                    if (status) {
                        handleClose();
                        setItemToUpdate({});
                        setItem(newObj);
                    }
                    else
                        setToastObj({ title: 'Error', message: getFirstErrorMessage(errors), show: true });
                });
    };

    return (
        <>
            <Modal.Header closeButton>
                <Modal.Title>{!_.isEmpty(itemToUpdate) ? "Update Homework" : "Add Homework"}</Modal.Title>
            </Modal.Header>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Modal.Body>
                    <Form.Group controlId="formBasicTitle">
                        <Form.Label>Title</Form.Label>
                        <Form.Control required type="text" defaultValue={_.get(itemToUpdate, 'title')} placeholder="Enter Homework Title" onChange={event => setTitle(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicLastName">
                        <Form.Label>Due Date</Form.Label>
                        <Form.Control required type="date" min={(new Date()).toLocaleString()} max={'3000-1-1'} defaultValue={_.get(itemToUpdate, 'due_date')} placeholder="Enter Due Date" onChange={event => setDueDate(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSubjects">
                        <Form.Label>Subjects</Form.Label>
                        <Form.Control as="select" value={_.get(itemToUpdate, 'subject_id')} onChange={event => setSelectedSubject(event.target.value)}>
                            {subjects.map((subject, index) => <option key={index} value={subject.id}>{subject.name}</option>)}
                        </Form.Control>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" type="submit">{!_.isEmpty(itemToUpdate) ? "Update" : "Save"}</Button>
                </Modal.Footer>
            </Form>
        </>
    );
}

const enhance = compose(
    WithPageContainer
);

export default enhance(AddEditHomework);