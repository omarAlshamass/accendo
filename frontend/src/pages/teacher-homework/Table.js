import { Button, Table } from 'react-bootstrap';
import { Pencil, Trash } from 'react-bootstrap-icons';
import _ from 'lodash';
import { apiCaller } from '../../common/utils/apiCaller';
import { homeWorkRoute } from '../../apis/homework';

const TableComponent = ({ list, setItem, setUpdateItem }) => {

    const deleteItem = (itemObj) => {
        apiCaller(homeWorkRoute + '/' + itemObj.id, {}, "DELETE")
            .then(({ status }) => {
                if (status) {
                    setItem(itemObj);
                }
            });
    };

    return (
        <Table striped bordered hover className="text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Subject</th>
                    <th>Title</th>
                    <th>Due Date</th>
                    <th>No.of Submission</th>
                    <th>Settings</th>
                </tr>
            </thead>
            <tbody>
                {list.map((item, itemIndex) => (
                    <tr key={itemIndex}>
                        <td>{_.get(item, 'id')}</td>
                        <td>{_.get(item, 'subject.name')}</td>
                        <td>{_.get(item, 'title')}</td>
                        <td>{_.get(item, 'due_date')}</td>
                        <td>{_.get(item, 'submitted_count') + '/' + _.get(item, 'users_count')}</td>
                        <td>
                            <Button variant="link" onClick={() => setUpdateItem(item)}>
                                <Pencil />
                            </Button>
                            <Button variant="link" onClick={() => deleteItem(item)}>
                                <Trash />
                            </Button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
}

export default TableComponent;