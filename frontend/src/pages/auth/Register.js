import { useEffect, useState } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { compose } from 'redux';
import WithPageContainer from '../../hocs/withPageContainer';
import { useHistory } from "react-router-dom";
import { isBlank, getFirstErrorMessage, waitForShowingToast } from '../../common/utils/helper';
import { registerRoute } from '../../apis/auth';

const Register = ({ setToastObj, apiCaller }) => {
    const history = useHistory();
    const [validated, setValidated] = useState(false);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [firstName, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [rule, setRule] = useState(1);

    useEffect(() => {
        localStorage.clear();
    }, []);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setValidated(true);

        if (form.checkValidity()) {
            apiCaller(registerRoute, { email, password, first_name: firstName, last_name: lastName, rule_id: rule })
                .then(({ message, errors, data }) => {
                    if (!isBlank(message) || !isBlank(errors))
                        setToastObj({ title: 'Error', message: getFirstErrorMessage(errors, message), toast: true });
                    else {
                        setToastObj({ title: 'Success', message: "Your new account created successfully, please login.", toast: true });
                        waitForShowingToast(() => {
                            history.push('/login');
                        });
                    }
                })
                .catch(error => {
                    setToastObj({ title: 'Error', message: error.message, toast: true });
                }
                );
        }
    };

    return <Container>
        <Row className="justify-content-md-center text-center">
            <Col>
                <h2>Register Page</h2>
            </Col>
        </Row>
        <Row className="justify-content-md-center">
            <Col lg="6">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Form.Group controlId="formBasicFirstName">
                        <Form.Label>First name</Form.Label>
                        <Form.Control required type="text" placeholder="Enter First Name" onChange={event => setFirstName(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicLastName">
                        <Form.Label>Last name</Form.Label>
                        <Form.Control required type="text" placeholder="Enter Last Name" onChange={event => setLastName(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control required type="email" placeholder="Enter email" onChange={event => setEmail(event.target.value)} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                         </Form.Text>
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control required type="password" placeholder="Password" onChange={event => setPassword(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Label>Rule</Form.Label>
                        <Form.Control as="select" onChange={event => setRule(event.target.value)}>
                            <option value="1">Student</option>
                            <option value="2">Teacher</option>
                        </Form.Control>
                    </Form.Group>
                    <Row>
                        <Col lg={8}>
                            <Button variant="primary" type="submit">
                                Submit
                    </Button>
                        </Col>
                        <Col lg={4} className="text-right">
                            <div>Back to<Button variant="link" onClick={() => history.push("/login")}>Login?</Button></div>
                        </Col>
                    </Row>
                </Form>
            </Col>
        </Row>
    </Container>
};

const enhance = compose(
    WithPageContainer
);

export default enhance(Register);