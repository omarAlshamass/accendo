<?php

namespace App\Repositories\User;

use App\Models\Rule;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function getStudentsIds(): Collection
    {
        return $this->model
            ->select('id')
            ->where('rule_id', Rule::STUDENT)
            ->get();
    }

    public function getStudentHomework(User $user): Collection
    {
        return $user->homework()->with('subject')->get();
    }

    public function submitStudentHomework(User $user, int $homeworkId): bool
    {
        return  $user->homework()->updateExistingPivot($homeworkId, ['submitted' => 1]);
    }
}
