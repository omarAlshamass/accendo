import _ from 'lodash';
import { useHistory } from 'react-router';

const STUDENT = 1;
const TEACHER = 2;

const isBlank = (string) => {
    return (_.isUndefined(string) || _.isNull(string) || string.trim().length === 0);
}

const getFirstErrorMessage = (errors, message) => {
    return typeof errors == 'object' ? !isBlank(_.values(errors)[0][0]) ? _.values(errors)[0][0] : message : errors;
}

const waitForShowingToast = (callback) => {
    setTimeout(callback, 2000);
}

const setLocalStorage = (key, value) => {
    localStorage.setItem(key, value);
}

const getUserRule = (ruleId) => {
    return ruleId === 1 ? 'student' : 'teacher';
}

const checkLoggedInUserRule = () => {
    const userRuleId = _.get(JSON.parse(localStorage.getItem('user_info')), 'rule_id', false);

    return userRuleId === STUDENT ? 1 : userRuleId === TEACHER ? 2 : false;
}

export {
    isBlank,
    getFirstErrorMessage,
    waitForShowingToast,
    setLocalStorage,
    getUserRule,
    checkLoggedInUserRule,
    TEACHER,
    STUDENT
}