<?php

namespace App\Repositories\Homework;

use App\Models\Homework;
use App\Repositories\BaseRepository;

class HomeworkRepository extends BaseRepository implements HomeworkRepositoryInterface
{
    /**
     * HomeworkRepository constructor.
     *
     * @param Homework $model
     */
    public function __construct(Homework $model)
    {
        parent::__construct($model);
    }

    public function getAllWithUsersAndSubjects(int $userId)
    {
        return $this->model
            ->select([
                'id',
                'title',
                'due_date',
                'subject_id'
            ])
            ->where('user_id', $userId)
            ->with([
                'subject' => function ($query) {
                    return $query->select(['id', 'name']);
                }
            ])
            ->withCount([
                'users',
                'users as submitted_count' => function ($query) {
                    $query->where('submitted', 1);
                }
            ])
            ->get();
    }

    public function assignHomeworkToSudents(int $homeworkId, $students)
    {
        return $this->model
            ->find($homeworkId)
            ->users()
            ->sync($students);
    }
}
