import { compose } from 'redux';
import withApiCaller from "./withApiCaller";
import withToast from "./withToast";

const WithPageContainer = compose(
    withToast,
    withApiCaller
);

export default WithPageContainer;