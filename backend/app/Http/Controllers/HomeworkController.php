<?php

namespace App\Http\Controllers;

use App\Http\Requests\HomeworkPostRequest;
use App\Http\Requests\HomeworkUpdateRequest;
use App\Repositories\Homework\HomeworkRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Traits\ResponseTrait;
use App\Traits\UserTrait;
use Exception;
use Illuminate\Support\Facades\DB;

class HomeworkController extends Controller
{
    use ResponseTrait, UserTrait;

    private $homeworkRepository;
    private $userRepository;

    public function __construct(
        HomeworkRepositoryInterface $homeworkRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->homeworkRepository = $homeworkRepository;
        $this->userRepository = $userRepository;
    }

    public function get()
    {
        return $this->response($this->homeworkRepository->getAllWithUsersAndSubjects($this->getUser()->id));
    }

    public function post(HomeworkPostRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $homework = $this->homeworkRepository
                    ->create(
                        array_merge(
                            $request->validated(),
                            ['user_id' => $this->getUser()->id]
                        )
                    );
                $this->homeworkRepository->assignHomeworkToSudents($homework->id, $this->userRepository->getStudentsIds());
            });
        } catch (Exception $e) {
            return $this->response(null, 'An error happened, please try again later.', 500);
        }

        return $this->response(null);
    }

    public function delete(int $id)
    {
        return $this->response($this->homeworkRepository->destroy($id));
    }

    public function update(int $id, HomeworkUpdateRequest $request)
    {
        return $this->response($this->homeworkRepository->update($id, $request->validated()));
    }

    public function getStudentHomework()
    {
        return $this->response($this->userRepository->getStudentHomework($this->getUser()));
    }

    public function submitStudentHomework(int $homeworkId)
    {
        if ($this->userRepository->submitStudentHomework($this->getUser(), $homeworkId))
            return $this->response(null);
        else
            return $this->response(null, "the homework doesn't exist", 500);
    }
}
