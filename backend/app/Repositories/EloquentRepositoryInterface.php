<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface EloquentRepositoryInterface
{
    /**
     * Create a new object
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * Get object by id
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;

    /**
     * Get all the objects
     * @return Collection
     */
    public function all($columns = null): Collection;

    /**
     * Update an object by id
     */

    public function update(int $id, array $data): bool;

    /**
     * Delete an object by id
     */
    public function destroy(int $id);
}
