<?php

namespace App\Http\Controllers;

use App\Repositories\Subject\SubjectRepositoryInterface;
use App\Traits\ResponseTrait;

class SubjectController extends Controller
{
    use ResponseTrait;

    public function get(SubjectRepositoryInterface $subjectRepository)
    {
        return $this->response($subjectRepository->all());
    }
}
