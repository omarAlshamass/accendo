import { useEffect, useState } from 'react';
import Table from './Table';
import Main from '../master/Main';
import withPageContainer from '../../hocs/withPageContainer';
import { compose } from 'redux';
import { studentHomeworkRoute } from '../../apis/homework';
import { Col, Row } from 'react-bootstrap';

const StudentHomework = ({ apiCaller }) => {
    const [list, setList] = useState([]);
    const [item, setItem] = useState({});

    useEffect(() => {
        apiCaller(studentHomeworkRoute, {}, "GET").then(({ data }) => setList(data));
    }, [item]);

    return (
        <Main moduleTitle="Teacher Module">
            <Row className="mt-1 ml-1 mr-1">
                <Col className="text-left">
                    <b>{"List Of Students' Homework"}</b>
                </Col>
            </Row>
            <Row className="mt-1 ml-1 mr-1">
                <Col>
                    {list.length > 0 && <Table setItem={setItem} list={list} />}
                    {list.length === 0 && <div>No homework yet.</div>}
                </Col>
            </Row>
        </Main >
    );
};

const enhance = compose(
    withPageContainer
);

export default enhance(StudentHomework);