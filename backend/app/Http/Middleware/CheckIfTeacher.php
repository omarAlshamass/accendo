<?php

namespace App\Http\Middleware;

use App\Models\Rule;
use App\Traits\ResponseTrait;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckIfTeacher
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
        if (data_get(Auth::user(), 'rule_id') == Rule::TEACHER) {
            return $next($request);
        } else
            return $this->response(null, "sorry, you don't have permission to access this route", 403);
    }
}
