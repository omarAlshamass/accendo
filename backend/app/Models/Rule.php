<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    use HasFactory;

    const STUDENT = 1;
    const TEACHER = 2;

    public function users()
    {
        return $this->hasMany(User::class, 'rule_id', 'id');
    }
}
