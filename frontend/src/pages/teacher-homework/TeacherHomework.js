import { useEffect, useState } from 'react';
import Modal from '../../common/components/Modal';
import Table from './Table';
import AddEditHomework from './AddEditHomework';
import Main from '../master/Main';
import withPageContainer from '../../hocs/withPageContainer';
import { compose } from 'redux';
import { homeWorkRoute } from '../../apis/homework';
import { Col, Row } from 'react-bootstrap';

const TeacherHomework = ({ apiCaller }) => {
    const [showAddHomeWorkForm, setShowAddHomeWorkForm] = useState(false);
    const [list, setList] = useState([]);
    const [item, setItem] = useState({});
    const [itemToUpdate, setItemToUpdate] = useState({});

    const setUpdateItem = (item) => {
        setItemToUpdate(item);
        setShowAddHomeWorkForm(true);
    }

    useEffect(() => {
        apiCaller(homeWorkRoute, {}, "GET").then(({ data }) => setList(data));
    }, [item]);

    return (
        <Main moduleTitle="Teacher Module">
            <Row className="mt-1 ml-1 mr-1">
                <Col className="text-left">
                    <b>List Of Homework</b>
                </Col>
                <Col className="text-right">
                    <Modal
                        show={showAddHomeWorkForm}
                        setShow={setShowAddHomeWorkForm}
                        buttonTitle="Add new Homework">
                        <AddEditHomework
                            setItemToUpdate={setItemToUpdate}
                            itemToUpdate={itemToUpdate}
                            setItem={setItem}
                            handleClose={() => setShowAddHomeWorkForm(false)}
                        />
                    </Modal>
                </Col>
            </Row>
            <Row className="mt-1 ml-1 mr-1">
                <Col>
                    {list.length > 0 && <Table setItem={setItem} setUpdateItem={setUpdateItem} list={list} />}
                    {list.length === 0 && <div>No homework yet.</div>}
                </Col>
            </Row>
        </Main >
    );
};

const enhance = compose(
    withPageContainer
);

export default enhance(TeacherHomework);